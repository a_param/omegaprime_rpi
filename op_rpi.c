/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: op_rpi.c
        Version:   v0.0
        Date:      08-01-2016
    ============================================================================
 */


/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */

#include "op_rpi.h"


/*
 ** ============================================================================
 **                           LOCAL DEFINES
 ** ============================================================================
 */


/* 
 ** ============================================================================
 **                   LOCAL EXPORTED FUNCTION DECLARATIONS
 ** ============================================================================
 */
void printWelcome (void);


/*
 ** ============================================================================
 **                       LOCAL VARIABLES 
 ** ============================================================================
 */


/*==============================================================================
 ** Function...: main
 ** Return.....: int
 ** Description: Main function 
 ** Created....: 21.05.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/

// TO BUILD: gcc -Wall -o test -pthread rpiLoRa.c -lwiringPi
// TO RUN: sudo ./test -D /dev/spidev0.0

int main (int argc, char *argv[])
{
    int pt_iret;
    int ret = 0;

    pthread_t pt_opak, pt_terminal, pt_wp_in_handler;
    
    const char *pt_opak_msg = "Kernel";
    const char *pt_term_msg = "Terminal";
    const char *pt_wp_in_handler_msg = "WP Input Handler";
    
    printWelcome();
    ret = init();
    errCodeHnl (2, ret);

    printf("Starting of the threads\n");
    
    printf ("   Creating thread: Kernel  ...............");
    pt_iret = pthread_create (&pt_opak, NULL, opak, (void*) pt_opak_msg);
    errCodeHnl (0, pt_iret);
    
    printf ("   Creating thread: Terminal  .............");
    pt_iret = pthread_create (&pt_terminal, NULL, terminal, (void*) pt_term_msg);
    errCodeHnl (0, pt_iret);
    
    printf ("   Creating thread: WP Input Handler  .....");
    pt_iret = pthread_create (&pt_wp_in_handler, NULL, wp_in_handler, (void*) pt_wp_in_handler_msg);
    errCodeHnl (0, pt_iret);
    
    pthread_join (pt_opak, NULL);
    pthread_join (pt_terminal, NULL);
    pthread_join (pt_wp_in_handler, NULL);
    
    exit (EXIT_SUCCESS);
    
    return 0;
   
}

