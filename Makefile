CC=gcc
CFLAGS=-c  -Wall 
LDFLAGS= 
SOURCES = op_rpi.c system/os/opak.c system/init.c system/terminal/terminal.c system/debug/printOuts.c system/wiringpi/wiring_pi.c system/wiringpi/wiring_pi_input_handler.c application/gameConsol.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=op_rpi

all: $(SOURCES) $(EXECUTABLE)
    
$(EXECUTABLE): $(OBJECTS) 
		$(CC) $(LDFLAGS) $(OBJECTS) -o $@ -pthread -lwiringPi

.c.o:
		$(CC) $(CFLAGS) $< -o $@
		
clean: 
	find . -type f | xargs -n 5 touch
	-$(RM) $(EXECUTABLE) $(OBJECTS) 
		