/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: nrf.c
        Version:   v0.0
        Date:      21-05-2015
    ============================================================================
 */

/*============================================================================*/
/*                           INCLUDE STATEMENTS
==============================================================================*/

#include "nrf.h"
#include "nrfBits.h"
#include "nrfCfg.h"

void isr_handler (void);



/*
 ** ============================================================================
 **                        GLOBAL VARABLES
 ** ============================================================================
 */

unsigned int frameDubManager[100]; // FIXME Magic number
static uint8_t rxMode = 1;

/* 
 ** ============================================================================
 **                   LOCAL FUNCTION DECLARATIONS
 ** ============================================================================
 */

void clockOut (uint8_t size, uint8_t *ptr);

/*==============================================================================
 ** Function...: nrf
 ** Return.....: GLOB_RET
 ** Description: initializes the pins used by NRF
 ** Created....: 19.07.2012 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/

GLOB_RET nrf (void)
{
    GLOB_RET ret = GLOB_SUCCESS;

    printf ("Initializing NRF905 interface \t");

    // nRF initial modes
    TX_EN (0); // Rx mode
    TRX_CE (0); // Standby mode
    PWR_UP (0); // Power down

    ret = initNRF ();

    return ret;
}

/*------------------------------------------------------------------------------
 ** Function...: nrfConf
 ** Return.....: GLOB_RET
 ** Description: configure nrf chip
 ** Created....: 25.06.2012 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/
GLOB_RET initNRF (void)
{
    char *nRFAddress = "MOTE";
    uint8_t nRFConfig[10];

    nRFConfig[0] = CH_NO_BYTE; //Sets center frequency together with HFREQ_PLL
    //Output power, Band 433 or 868/915 MHz
    nRFConfig[1] = PA_PWR_10dBm | HFREQ_PLL_433MHz | CH_NO_BIT8;
    nRFConfig[2] = TX_AFW_4BYTE | RX_AFW_4BYTE; //Tx and Rx Address width
    nRFConfig[3] = RX_PW_4BYTE; //RX payload width
    nRFConfig[4] = TX_PW_4BYTE; //Tx payload width
    nRFConfig[5] = nRFAddress[0];
    nRFConfig[6] = nRFAddress[1];
    nRFConfig[7] = nRFAddress[2];
    nRFConfig[8] = nRFAddress[3];
    nRFConfig[9] = CRC8_EN | XOF_16MHz;
    //CRC check. Crystal oscillator frequency.

    PWR_UP (1);
    //Write nRF configuration 
    SPIBurstWrite (WC, nRFConfig, 0x00, sizeof (nRFConfig) + 1);
    sleep (1);
    //Write TX Address
    SPIBurstWrite (WTA, (void *) nRFAddress, 0x00, sizeof (nRFAddress) + 1);

    TRX_CE (1);

    return GLOB_SUCCESS;
}

/*------------------------------------------------------------------------------
 ** Function...: setNRFpw
 ** Return.....: void
 ** Description: set payload width
 ** Created....: 25.06.2012 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/
void setNRFpw (uint8_t type, uint8_t pw)
{
    //configure RX payload width
    if (type == RX)
    {
        //Write to nRF configuration bit 3
        SPIBurstWrite (WC3, &pw, 0x00, sizeof (pw) + 1);

    }
        //configure TX payload width
    else if (type == TX)
    {
        //Write to nRF configuration bit 3
        SPIBurstWrite (WC4, &pw, 0x00, sizeof (pw) + 1);
    } // end else if
}



/*------------------------------------------------------------------------------
 ** Function...: TXPacket
 ** Return.....: void
 ** Description: transmit frame
 ** Created....: 25.06.2012 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/
GLOB_RET TXPacket (void)
{
    rxMode = 0;
    
    GLOB_RET ret = GLOB_SUCCESS;
    TRX_CE (1); // Start transmitting
    while (!digitalRead(DR));
    TX_EN (0);
    
    rxMode = 1;

    return ret;
}


/*------------------------------------------------------------------------------
 ** Function...: clockIn
 ** Return.....: int
 ** Description: clock payload in to nrf register
 ** Created....: 25.06.2012 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/
int clockIn (void* data, int size)
{
    char *d = (char *) data;
    TRX_CE (0);
    TX_EN (1);
    SPIBurstWrite (WTP, d, 0x00, size + 1);
    return 0;
}


/*------------------------------------------------------------------------------
 ** Function...: clockOut
 ** Return.....: void
 ** Description: clock payload out from nrf register
 ** Created....: 25.06.2012 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/
void clockOut (uint8_t size, uint8_t *ptr)
{
    uint8_t nRFConfig[size];
    uint8_t *tmp;

    memset(nRFConfig, 0x00, size);

    TRX_CE (0);
    tmp = SPIBurstWrite (0x24, nRFConfig, 0x00, sizeof (nRFConfig) + 1);
    TRX_CE (1);

    memcpy (ptr, tmp, size);
} 


/*==============================================================================
 ** Function...: transmitFrame
 ** Return.....: GLOB_RET
 ** Description: session
 ** Created....: 09.08.2012 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
GLOB_RET transmitFrame (void *data, uint8_t pw)
{
    GLOB_RET ret = GLOB_SUCCESS;
    
    rxMode = 0;

    setNRFpw (TX, pw);
    clockIn(data, pw); // Clock data in with SPI
    ret = TXPacket (); // Transmit

    rxMode = 1;

    return ret;
}


/*==============================================================================
 ** Function...: external interrupt for data ready
 ** Return.....: void
 ** Description:
 ** Created....: 19.07.2012 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void isr_handler (void)
{    
    if(rxMode)
    {
        uint8_t *rxFrame; 
        uint8_t src = 0;
        unsigned int seq = 0;
        
        rxFrame =  (uint8_t *)malloc(PACKET_SIZE);        
        clockOut(PACKET_SIZE,rxFrame); 

        memcpy(&src, rxFrame+1,1);
        memcpy(&seq, rxFrame+8, 2);
        
        if(seq^frameDubManager[src])
        { 
            frameDubManager[src] = seq;
            if(Sme.term.rxPktStats)
            {
                if(src == 1)
                {
                    Log.pls.avrRXDevice[0]++;
                }else if (src == 2)
                {
                    Log.pls.avrRXDevice[1]++;
                }
            }
            send_msg(0, 0, rxFrame, PACKET_SIZE);
        }
    }
}


