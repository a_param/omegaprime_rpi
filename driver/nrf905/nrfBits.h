#ifndef NRFBITS_H
#define	NRFBITS_H

// Port config for nRF905 ( pin function)
#define TX_EN(x)   digitalWrite(8, x)    //TX_EN=1 TX mode, TX_EN=0 RX mode
#define TRX_CE(x)   digitalWrite(9, x)    //Enables chip for receive and transmit
#define PWR_UP(x)   digitalWrite(7, x)    //Power up chip

#define CD  2         //Carrier Detect
#define AM  3         //Address Match
#define DR  0

#endif	/* NRFBITS_H */