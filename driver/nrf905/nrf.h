/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: nrf.h
        Version:   v0.0
        Date:      21-05-2015
    =============================================================================
*/


/*
** ==============================================================================
**                        INCLUDE STATEMENTS
** ==============================================================================
*/

#ifndef NRF_H
#define	NRF_H

#include "nrfBits.h"
#include "nrfCfg.h"

uint8_t nrf(void);
void setNRFpw(uint8_t type, uint8_t pw) ;
uint8_t initNRF(void);
void isr_handler(void);
uint8_t transmitFrame (void *data, uint8_t pw);



#endif	/* NRF_H */