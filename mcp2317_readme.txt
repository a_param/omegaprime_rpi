 Chip    | Hardwire Address|  I2C     |
Address  | A2  |  A1  | A0 | Address  |
---------------------------------------
   000   | GND |  GND | GND|   0x20   |
   001   | GND |  GND | 3V3|   0x21   |
   010   | GND |  3V3 | GND|   0x22   |
   011   | GND |  3V3 | 3V3|   0x23   |
   100   | 3V3 |  GND | GND|   0x24   |
   101   | 3V3 |  GND | 3V3|   0x25   |
   110   | 3V3 |  3V3 | GND|   0x26   |
   111   | 3V3 |  3V3 | 3V3|   0x27   |
   
   
   
   MCP
   [1] [5]
   [2] [6]
   [3] [7]
   [4] [8]
   
   MCP ID
   [1] -> 0x27
   [2] -> 0x26
   [3] -> 0x25
   [4] -> 0x24
   
   [5] -> 0x23
   [6] -> 0x22
   [7] -> 0x21
   [8] -> 0x20
   
   
   Pinnumber
   7 8
   6 9
   5 10
   4 11
   3 12
   2 13
   1 14
   0 15
   
   