/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: op_global_include.h
        Version:   v0.0
        Date:      11-01-2016
    =============================================================================
*/


/*
** ==============================================================================
**                        INCLUDE STATEMENTS
** ==============================================================================
*/

#ifndef OP_COM_IN_H
#define OP_COM_IN_H

#include <time.h>


/*
** ==============================================================================
**                        DEFINES AND ENUMS
** ==============================================================================
*/

/**
 * Enumeration containing different return types
 */
typedef enum {
    IN_BUFFER_IGNORE = 12,
    IN_BUFFER_EMPTY = 11,
    IN_BUFFER_FULL = 10,
    OUT_BUFFER_EMPTY = 9,
    OUT_BUFFER_FULL = 8,

    MAC_ACK_TIMEOUT = 7, /* Payload sent but did not receive ack */
    MAC_GOT_ACK = 6, /* Payload sent and received ack */
    MAC_CTS_TIMEOUT = 5, /* RTS sent but did not receive cts*/
    MAC_GOT_CTS = 4, /* RTS sent and received cts*/
    MAC_DIFS_WAIT_OK = 3, /* Waited DIFS without any CD*/
    MAC_BACKOFF_OK = 2, /* Did backoff without CD*/
    MAC_CARRIER_DETECTED = 1, /* Carrier detected */

    GLOB_SUCCESS = 0, /* The prosses was prossed with success */
    GLOB_FAILURE = -1, /* The prosses was prossed with some or more error */
    GLOB_ERROR_OUT_OF_RANGE_PARAM = -2, /* The parameter passed to the function is outside the valid range */
    GLOB_ERROR_INVALID_MESSAGE = -3, /* The given message does not identify a valid object */
    GLOB_ERROR_INVALID_PARAM = -4, /* The parameter passed to the function is invalid*/
    GLOB_ERROR_OUT_OF_HANDLES = -5, /* There is no free handle available*/
    GLOB_ERROR_INIT = -6, /* Initialisation when wrong */
    GLOB_WARNING_PARAM_NOT_SET = -20, /* There requiret parameter is not set */

    APP_CONF_FAILURE = -21,
} GLOB_RET;


/**
 * Enumeration containing different wiring_pi setup types
 */
typedef enum {
    WP_NRF_MCP = 3,
    WP_MCP23017 = 2,
    WP_NRF905 = 1,
    WP_DEFAULT = 0,
}WIRING_SETUP;


// Regular colors
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
#define KBLINK "\x1B[5m"
        
// Special forground colors
#define SFGORN "\x1B[38;5;202m"


/**
 * pt threads
 */
typedef struct {
    char kernel;
    char terminal;
    char tcp;
    char uart;
} ptThreads;

/**
 * Terminal input
 */
typedef struct {
    char startLog;
} terminalIn;

/**
 * System status
 */
typedef struct {
  ptThreads ptrd;
  terminalIn term;
} status;


/**
 * System level management entity
 */

typedef struct {
    
    status sysStatus;
    
}slme;


/*
** =============================================================================
**                   GLOBAL EXPORTED FUNCTION DECLARATIONS
** =============================================================================
*/

// Global management entity
extern slme Slme;




#endif //OP_COM_IN_H