#include "application.h"
#include <wiringPi.h>
#include <mcp23017.h>


void gameConsol_init(void)
{
    printf("Hello from gameConsol_init\n");
}

void gameConsol_input_event(uint8_t *msg, int len)
{
    uint8_t mcpId  = 0;
    uint8_t bAdd   = 0;
    uint8_t mcpPin = 0;
    uint8_t state  = 0;
    
    memcpy(&mcpId, msg, 1);
    memcpy(&mcpPin, msg+1, 1);
    memcpy(&state, msg+2, 1);

    if(0==mcpId) bAdd = 100;
    else if (1==mcpId) bAdd = 116;
    else if (2==mcpId) bAdd = 132;
    else if (3==mcpId) bAdd = 148;
    else if (4==mcpId) bAdd = 164;
    else if (5==mcpId) bAdd = 180;
    else if (6==mcpId) bAdd = 196;
    else if (7==mcpId) bAdd = 212;

    bAdd = bAdd + mcpPin;

    printf("In OS Task 0: id %d %d base %d state %d \n", mcpId, mcpPin, bAdd, state);

    mcp[mcpId].ioState[mcpPin] = state;

    if(state)
        digitalWrite(bAdd, LOW);
    else
        digitalWrite(bAdd, HIGH);
            
    
}