/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: terminal.c
        Version:   v0.0
        Date:      21-05-2015
    =============================================================================
 */


/*
 ** ==============================================================================
 **                        INCLUDE STATEMENTS
 ** ==============================================================================
 */

#include "terminal.h"

/*
 ** =============================================================================
 **                           LOCAL DEFINES
 ** =============================================================================
 */

#define MAX_ARGV 10

/* 
 ** =============================================================================
 **                   LOCAL EXPORTED FUNCTION DECLARATIONS
 ** =============================================================================
 */

uint16_t parseArgs (char *str, char *argv[]);

/*
 ** =============================================================================
 **                       LOCAL VARIABLES 
 ** =============================================================================
 */

FILE *fp;
uint16_t argc;
char *argv[MAX_ARGV];

/*==============================================================================
 ** Function...: terminal
 ** Return.....: void 
 ** Description: Terminal interface
 ** Created....: 21.05.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void *terminal (void *msg)
{
   char cmdStr[100];
   int flag = 0;
   
   Slme.sysStatus.ptrd.terminal = 1;
   
    while (1)
    {
        if(Slme.sysStatus.ptrd.kernel == 1 &&
           Slme.sysStatus.ptrd.terminal == 1 &&
           Slme.sysStatus.ptrd.tcp == 0 &&
           Slme.sysStatus.ptrd.uart == 0)
        {
            printf (">> ");
            fgets(cmdStr,100,stdin);

            argc = parseArgs(cmdStr, argv);

            if (argc)
            {
                if (strcmp (argv[0], "help") == 0)
                {
                    // list the commands supported
                    printf ("%s The following commands are supported:\n", KCYN);
                    printf ("  %s mcp conf\n", KCYN);
                    printf ("  %s mcp id\n", KCYN);
                    printf ("  %s mcp state\n", KCYN);
                    printf ("  %s mcp set ID PIN STATE eg. mcp set 0 2 1\n", KCYN);
                    printf ("  %s log on\off\n", KCYN);

                }
                
                else if (strcmp (argv[0], "mcp") == 0)
                {
                    if(argc>1)
                    {
                        if (0 == strcmp (argv[1], "conf"))
                        {
                            op_hexdump_mcp23017(0);
                        }
                        else if (0 == strcmp (argv[1], "id"))
                        {
                            op_hexdump_mcp23017(1);
                        }
                        else if (0 == strcmp (argv[1], "state"))
                        {
                            op_hexdump_mcp23017(2);
                        }
                        else if (0 == strcmp (argv[1], "set"))
                        {
                            uint8_t data[3] = {0};
                            
                            if(argc>4)
                            {
                                data[0]  = atoi(argv[2]);
                                data[1]  = atoi(argv[3]);
                                data[2]  = atoi(argv[4]);
                                
                                // MCP ID must be between 0 - 7 
                                if( data[0] < 0 || data[0] > 7 )
                                {
                                    printf ("%s\tError: MCP ID must be between 0 - 7\n", KRED);
                                }
                                // MCP IO pins must be between 0 - 15
                                else if( data[1] < 0 || data[1] > 15 )
                                {
                                    printf ("%s\tError: MCP IO pins must be between 0 - 15\n", KRED);
                                }
                                // MCP state must be between 0 or 1
                                else if( data[2] < 0 || data[2] > 1 )
                                {
                                    printf ("%s\tError: MCP state must be between 0 or 1\n", KRED);
                                }
                                else
                                {
                                    sendMail(0, 0, &data[0], 3);
                                }  
                            }
                            else
                            {
                                printf ("%s\tError: missing parameters: %s\n", KRED, argv[0]);
                            }
                        }
                        else if (0 == strcmp (argv[1], "test"))
                        {
                            uint8_t data[3] = {0};
                            data[0]  = atoi(argv[2]);
                            sendMail(1, 1, &data[0], 3);
                        }
                    }
                }
                else if (strcmp(argv[0], "log") == 0)
                {
                    if (argc > 1)
                    {
                        if (0 == strcmp(argv[1], "on"))
                        {
                            Slme.sysStatus.term.startLog = 1;

                            char filename[255];

                            time_t rawtime;
                            struct tm * timeinfo;

                            rawtime = time(0); // get current time
                            timeinfo = localtime(&rawtime); // get structure
                            sprintf(filename, "file_%02d%04d.log", timeinfo->tm_mon+1,
                                 timeinfo->tm_year+1900);

                            printf("%s\tTurning on LOGGING \n", KCYN);
                            fp = fopen(filename, "a");

                            time ( &rawtime );
                            timeinfo = localtime ( &rawtime );

                            fprintf(fp, 
                            "************************************************************************************\n"
                            "Project: OmegaPrime v.0.1\n"
                            "Author: Achuthan Paramanathan\n"        
                            "Time: %s"
                            "************************************************************************************\n\n\n",
                            asctime (timeinfo));
                            fflush(fp);
                        }
                        else if (0 == strcmp(argv[1], "off"))
                        {
                            printf("%s\tTurning LOGGING off\n", KCYN);
                            Slme.sysStatus.term.startLog = 0;
                            
                            time_t rawtime;
                            struct tm * timeinfo;
                            
                            time ( &rawtime );
                            timeinfo = localtime ( &rawtime );

                            fprintf(fp, 
                            "************************************************************************************\n"    
                            "Time: %s"
                            "************************************************************************************\n\n\n",
                            asctime (timeinfo));
                            fflush(fp);
                            fclose(fp);
                        }
                    }
                }
                else
                {
                    printf ("%s\tError: unknown cmd: %s\n", KRED, argv[0]);
                }

                printf ("%s", KNRM);
            }
        }
        else
        {
            if(!flag)
            {
                flag = 1;
                printf("\n Initializing pt-threads...wait..\n");
            }
            
        }
    }
}

/*==============================================================================
 ** Function...: parseArgs
 ** Return.....: void 
 ** Description: Parse userinputs 
 ** Created....: 21.05.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
uint16_t parseArgs (char *str, char *argv[])
{
    uint16_t i = 0;
    char *ch = str;

    if (*ch == '\r')
    {
        return 0;
    }

    while (*ch != '\0')
    {
        i++;

        /*Check if length exceeds*/
        if (i > MAX_ARGV)
        {
            printf ("Too many arguments\n");
            return 0;
        }
        argv[i - 1] = ch;

        while (*ch != ' ' && *ch != '\0' && *ch != '\r'&& *ch != '\n')
        {
            if (*ch == '"') // Allow space characters inside double quotes
            {
                ch++;
                argv[i - 1] = ch; // Drop the first double quote char
                while (*ch != '"')
                {
                    if (*ch == '\0' || *ch == '\r')
                    {
                        printf ("Syntax error\n");
                        return 0;
                    }
                    ch++; // Record until next double quote char
                }
                break;
            }
            else
            {
                ch++;
            }
        }
        if (*ch == '\r')
        {
            break;
        }
        if (*ch != '\0')
        {
            *ch = '\0';
            ch++;
            while (*ch == ' ')
            {
                ch++;
            }
        }
    }
    return i;
}