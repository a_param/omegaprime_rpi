/*  =============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    All rights reserved.
    =============================================================================
    This document contains proprietary information belonging to Achuthan Paramanathan.
    Passing on and copying of this document, use and communication of its contents 
    is not permitted without prior written authorisation.
    =============================================================================
    Revision Information:
        File name: printOuts.c
        Version:   v0.0
        Date:      21-05-2015
    =============================================================================
*/


/*
** ==============================================================================
**                        INCLUDE STATEMENTS
** ==============================================================================
*/

#include "../system.h"
 
/*
** =============================================================================
**                           LOCAL DEFINES
** =============================================================================
*/


/* 
** =============================================================================
**                   LOCAL EXPORTED FUNCTION DECLARATIONS
** =============================================================================
*/
void mcp23017_print_header(uint8_t f);

/*
** =============================================================================
**                       LOCAL VARIABLES 
** =============================================================================
*/


/*==============================================================================
** Function...: printWelcome
** Return.....: void 
** Description: Welcome msg
** Created....: 21.05.2015 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void printWelcome(void)
{
  int n;
  printf("\n\n\n");
  for (n=0; n<77; n++)
  {
    printf("%s*%s",SFGORN, KNRM);
  }
  printf("\n");
  printf("%s*%s",SFGORN, KNRM);
  for (n=0; n<15; n++)
  {
    printf(" ");
  }
  printf("OmegaPrime Terminal Interface v.0.1");
  for (n=0; n<25; n++)
  {
    printf(" ");
  }
  
  printf("%s*%s",SFGORN, KNRM);
  printf("\n");
  
  for (n=0; n<77; n++)
  {
    printf("%s*%s",SFGORN, KNRM);
  }
  printf("\n\n");
}


/*==============================================================================
** Function...: errCodeHnl
** Return.....: void 
** Description: Error code handler
** Created....: 21.05.2015 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void errCodeHnl(uint8_t funk, uint8_t retType)
{
  switch(funk)
  {
    case 0:
      if(0 == retType)
      {
        printf(".....................   [%sSuccess%s]\n", KGRN, KNRM);
      }
      else if (1 == retType)
      {
        printf(".....................   [%sFailed%s]\n", KRED, KNRM);
        printf("\t %s- pthread_create() return code %d\n%s",KRED, retType, KNRM);
        exit(EXIT_FAILURE);
      }
    break; 
    
    case 1:
      if(0 == retType)
      {
        printf("..................\t   [%sSuccess%s]\n", KGRN, KNRM);
      }
      else if (1 == retType)
      {
        printf("..................\t   [%sFailed%s]\n", KRED, KNRM);
      }
      else if (2 == retType)
      {
        printf(".................\t   [%sFailed%s]\n", KRED, KNRM);
        printf("\t\t%s- Error while opening the file\n%s",KRED, KNRM);
        
      }
      else if (3 == retType)
      {
        printf(".................\t   [%sFailed%s]\n", KRED, KNRM);
        printf("\t\t%s- Error while executing shell script\n%s",KRED, KNRM);
        
      }
    break;
    
    case 2:
      if(0 == retType)
      {
        printf("System initialization  ................."
               ".......................\t   [%sSuccess%s]\n", KGRN, KNRM);
      }
      else 
      {
         printf("System initialization  ................."
                ".......................\t   [%sFailed%s]\n", KRED,KNRM);
      }
    break; 
    
    case 3:
      if(0 == retType)
      {
        printf("............  [%sSuccess%s]\n", KGRN, KNRM);
      }
      else 
      {
        printf("............  [%sFailed%s]\n", KRED,KNRM);
      }
    break; 

    default : 
      printf("............ %s wrong return code\n%s", KRED,KNRM);
  }
  
  if(1==retType)
  {
    printf("%sSystem initialization filed\n%s", KRED, KNRM);
    exit(1);
  }
}


/*==============================================================================
 ** Function...: op_hexdump
 ** Return.....: void
 ** Description: 
 ** Created....: 29.12.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void op_hexdump(int level, const char *title, const void *buf, char len)
{
    char s[40];
    int i = 0, j = 0;
    char d[32] = { 0 };
    
    memcpy(d, buf, len);
    
    sprintf(s,"%s", title);
    printf(s);
    
    printf("\n");
	for (i = 0; i < len; i++)
	{
            sprintf(s," 0x%02x", d[i]);
            printf(s);
            j++;
            if (j == 8)
            {
                j = 0;
                printf("\n");
            }
	}
	printf("\n\n");
}


/*==============================================================================
 ** Function...: op_hexdump_mcp23017
 ** Return.....: void
 ** Description: 
 ** Created....: 14.01.2016 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void op_hexdump_mcp23017(uint8_t f)
{
    int i = 0, j = 0;
    
    mcp23017_print_header(f);
            
    for (i = 0; i < NUM_MCP23017; i++)
    {  
        printf("<#%d> %d  0x%x (%d) | ", i, mcp[i].base, mcp[i].addr, mcp[i].addr);
        for (j = 0; j < 16; j++)
        {
            if(0==f)
            {
                printf(" %d", mcp[i].ioConf[j]);
            }
            else if(1==f)
            {
               printf(" %d", mcp[i].ioNum[j]); 
            }
            else if(2==f)
            {
               printf(" %d", mcp[i].ioState[j]); 
            }
        }
        printf("\n");
    }
}


/*==============================================================================
 ** Function...: mcp23017_print_header
 ** Return.....: void
 ** Description: 
 ** Created....: 14.01.2016 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void mcp23017_print_header(uint8_t f)
{
    if(0==f || 2==f)
    {
        printf("------------------------------------------------------\n");
        printf("MCP# BASE ADDR (hex)|  0 1 2 3 4 5 6 7 8 9 A B C D E F\n");
        printf("------------------------------------------------------\n");
    }
    else if(1==f)
    {
        printf("---------------------------------------------------------------"
                "-----------------------\n");
        printf("MCP# BASE ADDR (hex)|  000 001 002 003 004 005 006 007 008 009 "
                "010 011 012 013 014 015\n");
        printf("---------------------------------------------------------------"
                "-----------------------\n");
    }


}