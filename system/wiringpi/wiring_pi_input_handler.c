#include <errno.h>
#include <wiringPi.h>
#include <mcp23017.h>
#include <stdio.h>

#include "../system.h"



void *wp_in_handler(void *msg)
{
    int i = 0, j = 0;
    uint8_t data[3] = {0};
    
    sleep(3);
    
    printf("MCP Input handler Ready!\n");
     
    while (1) 
    {
        for (i = 0; i < NUM_MCP23017; i++)
        {
            for (j = 0; j < 16; j++)
            {
                if (1 == mcp[i].ioConf[j])
                {
                    if(!digitalRead(mcp[i].base+j))
                    {
                        printf("INPUT %d %d %d %d\n", mcp[i].addr, mcp[i].base+j  ,mcp[i].ioConf[j], mcp[i].ioState[j] );
   
                        data[0]  = i;
                        data[1]  = j;
                        
                        if(mcp[i].ioState[j])
                        {
                            mcp[i].ioState[j] = 0;
                            data[2]  = 0;
                        }
                        else
                        {
                            mcp[i].ioState[j] = 1;
                            data[2]  = 1;
                        }
                                           
                        sendMail(2, 0, &data[0], 3);
                        usleep(250000);
                    }
                }
            }
        }
    }
}