/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: wiring_pi.h
        Version:   v0.0
        Date:      11-01-2016
    =============================================================================
*/


/*
** ==============================================================================
**                        INCLUDE STATEMENTS
** ==============================================================================
*/

#ifndef WIRING_H
#define WIRING_H
#include <unistd.h>
#include <stdlib.h>

/*
** ==============================================================================
**                        DEFINES AND ENUMS
** ==============================================================================
*/

#define NUM_MCP23017 8
#define BASE_MCP23017 100
#define START_ADD_MCP23017 0x20


typedef struct{
  uint8_t addr;
  uint8_t base;
  uint8_t ioConf[16];
  uint8_t ioState[16];
  uint8_t ioNum[16];
}mcp23017_conf;


/*
** =============================================================================
**                   GLOBAL EXPORTED FUNCTION DECLARATIONS
** =============================================================================
*/

int wiringPi(WIRING_SETUP gpioSetup);
void *wp_in_handler(void *msg);


/*
 ** ==========================================================================
 **                       Extern Global variables
 ** ==========================================================================
 */


extern mcp23017_conf mcp[NUM_MCP23017];
#endif //WIRING_H