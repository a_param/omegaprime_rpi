/*  =============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    All rights reserved.
    =============================================================================
    This document contains proprietary information belonging to Achuthan Paramanathan.
    Passing on and copying of this document, use and communication of its contents 
    is not permitted without prior written authorisation.
    =============================================================================
    Revision Information:
        File name: wiring_pi.c
        Version:   v0.0
        Date:      11-01-2016
    =============================================================================
 */


/*
 ** =============================================================================
 **                        INCLUDE STATEMENTS
 ** =============================================================================
 */

#include <errno.h>
#include <wiringPi.h>
#include <mcp23017.h>
#include <stdio.h>

#include "../system.h"

/*
 ** =============================================================================
 **                           LOCAL DEFINES
 ** =============================================================================
 */


/* 
 ** =============================================================================
 **                   LOCAL EXPORTED FUNCTION DECLARATIONS
 ** =============================================================================
 */
int wiringNRFInit(void);
int wiringMCP2317Init(void);
void isr_handler(void);

/*
 ** =============================================================================
 **                       LOCAL VARIABLES 
 ** =============================================================================
 */

mcp23017_conf mcp[NUM_MCP23017];

/*==============================================================================
 ** Function...: wiringPi
 ** Return.....: int 
 ** Description: main wiring pi function
 ** Created....: 21.05.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
int wiringPi(WIRING_SETUP gpioSetup)
{
    int ret = 0;

    printf("     Initializing WiringPi \n");
    // Initiate the wiringPi up and running
    if (wiringPiSetup() < 0)
    {
        fprintf(stderr, "\n\t Unable to setup wiringPi: %s\n", strerror(errno));
        return 1;
    }

    if (WP_NRF905 == gpioSetup)
    {
        ret = wiringNRFInit();
    }
    else if (WP_MCP23017 == gpioSetup)
    {
        ret = wiringMCP2317Init();
    }
    else if (WP_NRF_MCP == gpioSetup)
    {
        ret = wiringMCP2317Init();
        if (ret) return 1;
        ret = wiringNRFInit();
    }
    else
    {
        printf("%sError: unknown wiring setup command%s]\n", KRED, KNRM);
    }

    return ret;
}

/*==============================================================================
 ** Function...: wiringMCP2317Init
 ** Return.....: GLOB_RET
 ** Description: wiring init function for MCP2317 module 
 ** Created....: 19.07.2012 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
int wiringMCP2317Init(void)
{
    char ch;
    int i = 0, j = 0;
    int ret = 1;

    int totalIn = 0, totalOut = 0;
    FILE *fp;

    printf("\t - Setup interface for mcp23017\t ....");

    ret = system("./startI2C.sh");

    if (ret != 0)
    {
        errCodeHnl(1, 3);
        return 1;
    }

    fp = fopen("mcp2317_conf.txt", "r");
    if (fp == NULL)
    {
        errCodeHnl(1, 2);
        return 1;
    }

    errCodeHnl(1, 0);

    while ((ch = fgetc(fp)) != EOF)
    {
        if (ch == 'a') break;
        else if (ch == '\n')
        {
            i++;
            j = 0;
        }
        else
        {
            mcp[i].ioConf[j] = atoi(&ch);
            j++;
        }
    }
    fclose(fp);

    for (i = 0; i < NUM_MCP23017; i++)
    {
        mcp[i].addr = START_ADD_MCP23017 + i;

        if (0 == i)
        {
            mcp[i].base = BASE_MCP23017;
        }
        else
        {
            mcp[i].base = mcp[i - 1].base + 16;
        }
        
        mcp23017Setup(mcp[i].base, mcp[i].addr);

        for (j = 0; j < 16; j++)
        {
            mcp[i].ioNum[j] = mcp[i].base + j;
            mcp[i].ioState[j] = 0;

            if (1 == mcp[i].ioConf[j])
            {
                pinMode(mcp[i].ioNum[j], INPUT);
                pullUpDnControl (mcp[i].ioNum[j], PUD_UP);
                totalIn++;
            }
            else if (0 == mcp[i].ioConf[j])
            {
                pinMode(mcp[i].ioNum[j], OUTPUT);
                totalOut++;
            }
        }
    }

    printf("\t - Total MCP23017 In/OUT conf.: IN(%d) OUT(%d)\n", totalIn, totalOut);

    return 0;
}

/*==============================================================================
 ** Function...: wiringNRFInit
 ** Return.....: GLOB_RET
 ** Description: wiring init function for NRF905 module 
 ** Created....: 19.07.2012 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
int wiringNRFInit(void)
{
    printf("\t - Setup interface for NRF905\t ....");

    // set Pin 17/0 generate an interrupt on high-to-low transitions
    // and attach myInterrupt() to the interrupt
    if (wiringPiISR(DR, INT_EDGE_RISING, &isr_handler) < 0)
    {
        fprintf(stderr, "\n\t Unable to setup ISR: %s\n", strerror(errno));
        printf("... [%sFailed%s]\n", KRED, KNRM);
        return 1;
    }

    pinMode(8, OUTPUT); // Set WiP#8 as TX_EN
    pinMode(0, INPUT); // Set WiP#0 as DR
    pinMode(2, INPUT); // Set WiP#2 as CD
    pinMode(3, INPUT); // Set WiP#3 as AM
    pinMode(7, OUTPUT); // Set WiP#7 as PWR_UP 
    pinMode(9, OUTPUT); // Set WiP#9 as TRX_CE

    pinMode(1, OUTPUT); // Set Wip#1 as CSN
    pinMode(6, OUTPUT); // Set Wip#1 as CSN

    errCodeHnl(1, 0);
    return 0;
}

void isr_handler(void)
{

}