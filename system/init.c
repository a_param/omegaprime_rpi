/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: init.c
        Version:   v1.0
        Date:      25-04-2015
    ============================================================================
 */

/*
 ** =============================================================================
 **                                  INCLUDE STATEMENTS
 ** =============================================================================
 */

#include "system.h"

/*
 ** =============================================================================
 **                   Private FUNCTION DECLARATIONS
 ** =============================================================================
 */
void managementEntityInit(void);


/*
 ** =============================================================================
 **                        GLOBAL VARABLES
 ** =============================================================================
 */

slme Slme;


/*==============================================================================
 ** Function...: init
 ** Return.....: uint8_t
 ** Description: Main init function
 ** Created....: 19.07.2012 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
uint8_t init(void)
{
    int ret = 0; 
    
    printf("Start system initialization\n");
    managementEntityInit();
    ret = wiringPi(WP_MCP23017);
    
    return ret;
}


/*==============================================================================
 ** Function...: managementEntityInit
 ** Return.....: void
 ** Description: Initialization of management entities
 ** Created....: 19.07.2012 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void managementEntityInit(void)
{
    Slme.sysStatus.ptrd.kernel   = 0;
    Slme.sysStatus.ptrd.tcp      = 0;
    Slme.sysStatus.ptrd.terminal = 0;
    Slme.sysStatus.ptrd.uart     = 0;
    
    Slme.sysStatus.term.startLog = 0;
    
}