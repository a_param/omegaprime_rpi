/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: opak.h
        Version:   v0.0
        Date:      08-01-2016
    ============================================================================
*/

 
/*
** =============================================================================
**                        INCLUDE STATEMENTS
** =============================================================================
*/

#ifndef OPAK_H
#define OPAK_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "../system.h"


/*
** =============================================================================
**                                   DEFINES
** =============================================================================
*/

#define RING_BUF_SIZE 1000
#define IRING_BUF_SIZE 500
#define NR_PRIO 5
#define NR_INTR 7
#define LOCK lock  /* disable_interrupt(); */
#define UNLOCK unlock /* enable_interrupt(); */


/*
** =============================================================================
**                   GLOBAL EXPORTED FUNCTION DECLARATIONS
** =============================================================================
*/

void *opak(void *msg) ;
int sendMail(int rcv, int prio, uint8_t *msgP, int len);
#endif //OPAK_H