/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: opak.c
        Version:   v0.0
        Date:      08-01-2016
    ============================================================================
 */


/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */

#include <unistd.h>
#include <wiringPi.h>
#include <mcp23017.h>

#include "opak.h"


/*
 ** ============================================================================
 **                           LOCAL DEFINES
 ** ============================================================================
 */


/* 
 ** ============================================================================
 **                   LOCAL EXPORTED FUNCTION DECLARATIONS
 ** ============================================================================
 */
void printWelcome (void);


/*
 ** ============================================================================
 **                       LOCAL VARIABLES 
 ** ============================================================================
 */

typedef struct {
    int receiver, sender;
    int len;
} msg_hd_tp;

char buf[NR_PRIO][RING_BUF_SIZE];
int r[NR_PRIO], w[NR_PRIO], cnt[NR_PRIO];

int running = -1;
int nr_task = 0;


/*
 ** =============================================================================
 **                       IMPLEMENTATION
 ** =============================================================================
 */


/*==============================================================================
 ** Function...: sendMail
 ** Parameter..: receiver, which task shall this mail be  pass to
 ** Parameter..: prio, processing prio
 ** Parameter..: msg, message data
 ** Parameter..: len, length of the data
 ** Return.....: 0 on success, -1 invalid prio, -2 invalid data size
 ** Description: Receives and handles the mail sent by different process
 ** Created....: 21.05.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
int sendMail(int rcv, int prio, uint8_t *msgP, int len) {
    msg_hd_tp hdr;
    char * index;
    int i, dlgt, wi;
    char *msg = (char *) msgP;

    if (NR_PRIO <= prio)
        return -1;

    dlgt = len + (int) sizeof (msg_hd_tp);
    if ((RING_BUF_SIZE - cnt[prio]) < dlgt)
        return -2; 

    hdr.receiver = rcv;
    hdr.sender = running;
    hdr.len = len;
    
    wi = w[prio]; 
    index = &(buf[prio][0]);

    for (i = 0; i< sizeof (msg_hd_tp); i++) {
        *(index + wi++) = *((char *) (&(hdr)) + i);
        if (RING_BUF_SIZE <= wi)
            wi = 0;
    }

    if (0 < len) {
        for (i = 0; i < len; i++) {
            *(index + wi++) = *(msg + i);
            if (RING_BUF_SIZE <= wi)
                wi = 0;
        }
    }

    cnt[prio] += (int) sizeof (msg_hd_tp) + len;
    w[prio] = wi; 

    return 0; 
}


/*==============================================================================
 ** Function...: mailTask
 ** Parameter..: src, who is the sender, used for back trace
 ** Parameter..: rcv, which task it is for
 ** Parameter..: prio, task priority
 ** Parameter..: msg, data message
 ** Parameter..: len, length of the data
 ** Return.....: 0 on success, 1 else
 ** Description: Mail processor 
 ** Created....: 21.05.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
int mailTask(int src, int rcv, int prio, uint8_t *msg, int len) 
{
    uint8_t mcpId  = 0;
    uint8_t mcpPin = 0;
    uint8_t bAdd   = 0;
    uint8_t state  = 0;
    
    int i = 0, j = 0;
    
    if(Slme.sysStatus.term.startLog)
    {
        time_t rawtime;
        struct tm * timeinfo;
        rawtime = time(0); // get current time
        timeinfo = localtime(&rawtime); // get structure
        
        fprintf(fp,"%s: OPAK mailTask | src %d rcv %d prio %d len %d\n", 
                asctime (timeinfo),src, rcv, prio, len);
        fflush(fp);
    }
            
    switch (rcv) 
    {
        case 0:
            
            memcpy(&mcpId, msg, 1);
            memcpy(&mcpPin, msg+1, 1);
            memcpy(&state, msg+2, 1);
            
            if(0==mcpId) bAdd = 100;
            else if (1==mcpId) bAdd = 116;
            else if (2==mcpId) bAdd = 132;
            else if (3==mcpId) bAdd = 148;
            else if (4==mcpId) bAdd = 164;
            else if (5==mcpId) bAdd = 180;
            else if (6==mcpId) bAdd = 196;
            else if (7==mcpId) bAdd = 212;
            
            bAdd = bAdd + mcpPin;
            
            printf("In OS Task 0: id %d %d base %d state %d \n", mcpId, mcpPin, bAdd, state);
            
            mcp[mcpId].ioState[mcpPin] = state;

            if(state)
                digitalWrite(bAdd, LOW);
            else
                digitalWrite(bAdd, HIGH);
            
            return 0; 
            break;
        
        case 1:
            printf("In OS Task 1: %s \n", msg);
            
            memcpy(&mcpId, msg, 1);
            
            if(0==mcpId) bAdd = 100;
            else if (1==mcpId) bAdd = 116;
            else if (2==mcpId) bAdd = 132;
            else if (3==mcpId) bAdd = 148;
            else if (4==mcpId) bAdd = 164;
            else if (5==mcpId) bAdd = 180;
            else if (6==mcpId) bAdd = 196;
            else if (7==mcpId) bAdd = 212;
            
            printf("In OS Task 0: id %d %d base %d state %d \n", mcpId, mcpPin, bAdd, state);
            
            for(i=0; i<mcpId+1;i++)
            {
                for ( j = bAdd; j<bAdd+7; j++)
                {
                    printf("In OS Task 0: id %d %d base %d state %d \n", mcpId, mcpPin, bAdd, state);
                    digitalWrite(bAdd, LOW);
                    sleep(1);
                    digitalWrite(bAdd, HIGH);
                    sleep(1);
                    bAdd++;
                    
                }
            }
            
            return 0; 
            break;
        
        case 2:
            printf("In OS Task 2 \n");
            gameConsol_input_event(msg, len);
            return 0; 
            break;

        case 3:
            printf("In OS Task 3 \n");
            return 0; 
            break;

        case 4:
            printf("In OS Task 4 \n");
            return 0; 
            break;
        default: printf("Error: Invalid Mail Task!\n");
            exit(1);
    }
}

/*==============================================================================
 ** Function...: osInit
 ** Parameters.: int nr_tasks, how meany task do we need
 ** Return.....: void
 ** Description: Initialization of the OS 
 ** Created....: 21.05.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void osInit(int nr_tasks) {
    int i;
    
    Slme.sysStatus.ptrd.kernel = 1;
            
    nr_task = nr_tasks;

    for (i = 0; i < NR_PRIO; i++) {
        r[i] = w[i] = cnt[i] = 0;
    }
}


/*==============================================================================
 ** Function...: osLock
 ** Parameters.: void
 ** Return.....: void
 ** Description: lock the function, e.g., no interrupt 
 ** Created....: 21.05.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void osLock(void)
{
    
}


/*==============================================================================
 ** Function...: osUnlock
 ** Parameters.: void
 ** Return.....: void
 ** Description: Release the lock
 ** Created....: 21.05.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void osUnlock(void)
{
    
}


/*==============================================================================
 ** Function...: opak
 ** Parameters.: void *msg, message from initiator (in this case the main func)
 ** Return.....: void
 ** Description: Main function 
 ** Created....: 21.05.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void *opak(void *msg) 
{
    static msg_hd_tp hdr;
    static int priority;
    static uint8_t data[RING_BUF_SIZE];
    int prio, i, ri, lgt;
    char * index;

    osInit(5);
    
    while (1) {
xxx:
        for (prio = 0; prio < NR_PRIO; prio++) {
            osLock();
            if (cnt[prio]) { 
                priority = prio;
                ri = r[prio]; 
                index = &(buf[prio][0]); 

                for (i = 0; i< sizeof (msg_hd_tp); i++) {
                    *(((char*) (&hdr)) + i) = *((char *) index + ri++);
                    if (RING_BUF_SIZE <= ri)
                        ri = 0;
                }

                lgt = hdr.len;
                for (i = 0; i < lgt; i++) {
                    data[i] = *((char *) index + ri++);
                    if (RING_BUF_SIZE <= ri)
                        ri = 0;
                }
                
                cnt[prio] -= ((int) sizeof (msg_hd_tp) + lgt);
                r[prio] = ri;

                running = hdr.receiver;

                osUnlock();
                mailTask(hdr.sender, running, priority, data, lgt);
goto xxx;
            }
            osUnlock();
        }
    }
}